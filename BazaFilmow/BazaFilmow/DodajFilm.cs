﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace BazaFilmow
{
    public partial class DodajFilm : Form
    {
        private Main mainForm = null;
        public DodajFilm(Main m)
        {
            InitializeComponent();
            mainForm = m;
        }
        public DodajFilm()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Title = "Wybierz plakat";
            dlg.Filter = "Pliki JPG/JPEG|*.jpg;*.jpeg|Pliki PNG|*.png|Pliki GIF|*.gif|Pliki BMP|*.bmp|Wszystkie pliki|*.*";
            dlg.FilterIndex = 1;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBoxPlakat.Image = new Bitmap(Image.FromFile(dlg.FileName));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxTytul.Text == "" || textBoxRok.Text == "" || comboBoxGatunek.SelectedItem.ToString() == "wybierz gatunek" || textBoxCzasTrwania.Text == "")
            {
                MessageBox.Show("Uzupełnij wymagane pola oznaczone \"*\" !");
            }
            else
            {
                try
                {
                    string sciezka = @".\okladki\" + textBoxTytul.Text + ".jpg";
                    DirectoryInfo folder = new DirectoryInfo(@".\okladki");
                    folder.Create();

                    if (textBoxOpis.Text == "")
                    {
                        textBoxOpis.Text = "";
                    }

                    Film f = new Film(textBoxTytul.Text, Convert.ToInt32(textBoxRok.Text), comboBoxGatunek.SelectedItem.ToString(), textBoxCzasTrwania.Text, comboBoxOcena.SelectedItem.ToString(), sciezka, textBoxOpis.Text);
                    mainForm.DodaFilm(f);
                    mainForm.DodajFilmDoDataGridView(f);
                    pictureBoxPlakat.Image.Save(@".\okladki\" + textBoxTytul.Text + ".jpg", ImageFormat.Jpeg); 
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Błąd: " + ex.Message);
                }
            }
        }

        private void comboBoxOcena_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxOcena.SelectedValue = 1;
        }

    }
}
