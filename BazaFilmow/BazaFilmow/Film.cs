﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace BazaFilmow
{
    public class Film
    {
        private string Tytul, Gatunek, Ocena, CzasTrw, Plakat, Opis;
        private int Rok;

        public Film() { Tytul = ""; Rok = 0; Ocena = ""; Gatunek = ""; CzasTrw = ""; Plakat = ""; Opis = ""; }

        public Film(string tytul_, int rok_, string gatunek_, string czastrw_, string ocena_, string plakat_, string opis_) {
            Tytul = tytul_; Rok = rok_; Ocena = ocena_; Gatunek = gatunek_; CzasTrw = czastrw_; Plakat = plakat_; Opis = opis_;
        }

        public string tytul { get { return Tytul; } set { Tytul = value; } }
        public string gatunek { get { return Gatunek; } set { Gatunek = value; } }
        public int rok { get { return Rok; } set { Rok = value; } }
        public string ocena { get { return Ocena; } set { Ocena = value; } }
        public string czastrwania { get { return CzasTrw; } set { CzasTrw = value; } }
        public string plakat { get { return Plakat; } set { Plakat = value; } }
        public string opis { get { return Opis; } set { Opis = value; } }
    }
}
