﻿namespace BazaFilmow
{
    partial class PokazInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PokazInfo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelOpis = new System.Windows.Forms.Label();
            this.labelCzasTrwania = new System.Windows.Forms.Label();
            this.labelGatunek = new System.Windows.Forms.Label();
            this.labelRok = new System.Windows.Forms.Label();
            this.pictureBoxPlakaT = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTytul = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlakaT)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Controls.Add(this.labelOpis);
            this.groupBox1.Controls.Add(this.labelCzasTrwania);
            this.groupBox1.Controls.Add(this.labelGatunek);
            this.groupBox1.Controls.Add(this.labelRok);
            this.groupBox1.Controls.Add(this.pictureBoxPlakaT);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelTytul);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(576, 266);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informacje o filmie";
            // 
            // labelOpis
            // 
            this.labelOpis.AutoSize = true;
            this.labelOpis.Location = new System.Drawing.Point(191, 130);
            this.labelOpis.Name = "labelOpis";
            this.labelOpis.Size = new System.Drawing.Size(50, 13);
            this.labelOpis.TabIndex = 11;
            this.labelOpis.Text = "labelOpis";
            // 
            // labelCzasTrwania
            // 
            this.labelCzasTrwania.AutoSize = true;
            this.labelCzasTrwania.Location = new System.Drawing.Point(267, 102);
            this.labelCzasTrwania.Name = "labelCzasTrwania";
            this.labelCzasTrwania.Size = new System.Drawing.Size(90, 13);
            this.labelCzasTrwania.TabIndex = 10;
            this.labelCzasTrwania.Text = "labelCzasTrwania";
            // 
            // labelGatunek
            // 
            this.labelGatunek.AutoSize = true;
            this.labelGatunek.Location = new System.Drawing.Point(263, 78);
            this.labelGatunek.Name = "labelGatunek";
            this.labelGatunek.Size = new System.Drawing.Size(70, 13);
            this.labelGatunek.TabIndex = 9;
            this.labelGatunek.Text = "labelGatunek";
            // 
            // labelRok
            // 
            this.labelRok.AutoSize = true;
            this.labelRok.Location = new System.Drawing.Point(263, 53);
            this.labelRok.Name = "labelRok";
            this.labelRok.Size = new System.Drawing.Size(49, 13);
            this.labelRok.TabIndex = 8;
            this.labelRok.Text = "labelRok";
            // 
            // pictureBoxPlakaT
            // 
            this.pictureBoxPlakaT.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBoxPlakaT.BackgroundImage = global::BazaFilmow.Properties.Resources.brakzdjecia;
            this.pictureBoxPlakaT.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxPlakaT.Name = "pictureBoxPlakaT";
            this.pictureBoxPlakaT.Size = new System.Drawing.Size(172, 241);
            this.pictureBoxPlakaT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPlakaT.TabIndex = 7;
            this.pictureBoxPlakaT.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(191, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Czas trwania:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(191, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Gatunek:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Premiera:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tytuł:";
            // 
            // labelTytul
            // 
            this.labelTytul.AutoSize = true;
            this.labelTytul.Location = new System.Drawing.Point(263, 29);
            this.labelTytul.Name = "labelTytul";
            this.labelTytul.Size = new System.Drawing.Size(52, 13);
            this.labelTytul.TabIndex = 2;
            this.labelTytul.Text = "labelTytul";
            // 
            // PokazInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 290);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PokazInfo";
            this.Text = "PokazInfo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlakaT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelTytul;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBoxPlakaT;
        private System.Windows.Forms.Label labelOpis;
        private System.Windows.Forms.Label labelCzasTrwania;
        private System.Windows.Forms.Label labelGatunek;
        private System.Windows.Forms.Label labelRok;

    }
}