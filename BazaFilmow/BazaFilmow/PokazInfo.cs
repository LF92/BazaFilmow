﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BazaFilmow
{
    public partial class PokazInfo : Form
    {
        public PokazInfo(Film f)
        {
            InitializeComponent();
            labelTytul.Text = f.tytul;
            labelRok.Text = f.rok.ToString();
            labelGatunek.Text = f.gatunek;
            labelCzasTrwania.Text = f.czastrwania;
            labelOpis.Text = f.opis;
            pictureBoxPlakaT.ImageLocation = f.plakat;
        }

        public PokazInfo()
        {
            InitializeComponent();
        }
    }
}
