﻿namespace BazaFilmow
{
    partial class DodajFilm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DodajFilm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTytul = new System.Windows.Forms.TextBox();
            this.textBoxRok = new System.Windows.Forms.TextBox();
            this.comboBoxGatunek = new System.Windows.Forms.ComboBox();
            this.textBoxCzasTrwania = new System.Windows.Forms.TextBox();
            this.comboBoxOcena = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxOpis = new System.Windows.Forms.TextBox();
            this.pictureBoxPlakat = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlakat)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tytuł filmu*:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Rok premiery*:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Gatunek*:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ocena:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Czas trwania*:";
            // 
            // textBoxTytul
            // 
            this.textBoxTytul.Location = new System.Drawing.Point(92, 38);
            this.textBoxTytul.Name = "textBoxTytul";
            this.textBoxTytul.Size = new System.Drawing.Size(334, 20);
            this.textBoxTytul.TabIndex = 5;
            // 
            // textBoxRok
            // 
            this.textBoxRok.Location = new System.Drawing.Point(92, 61);
            this.textBoxRok.Name = "textBoxRok";
            this.textBoxRok.Size = new System.Drawing.Size(148, 20);
            this.textBoxRok.TabIndex = 6;
            // 
            // comboBoxGatunek
            // 
            this.comboBoxGatunek.AccessibleName = "";
            this.comboBoxGatunek.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboBoxGatunek.FormattingEnabled = true;
            this.comboBoxGatunek.Items.AddRange(new object[] {
            "Komiedia",
            "Horror",
            "Thriller",
            "Sensacyjny",
            "Dramat",
            "Erotyczny",
            "Biograficzny",
            "Western",
            "Akcja",
            "Sci-Fi",
            "Surrealistyczny",
            "Psychologiczny",
            "Przygodowy",
            "Animowany",
            "Historyczny",
            "Romans"});
            this.comboBoxGatunek.Location = new System.Drawing.Point(92, 84);
            this.comboBoxGatunek.Name = "comboBoxGatunek";
            this.comboBoxGatunek.Size = new System.Drawing.Size(148, 21);
            this.comboBoxGatunek.TabIndex = 7;
            this.comboBoxGatunek.Text = "wybierz gatunek";
            // 
            // textBoxCzasTrwania
            // 
            this.textBoxCzasTrwania.Location = new System.Drawing.Point(92, 110);
            this.textBoxCzasTrwania.Name = "textBoxCzasTrwania";
            this.textBoxCzasTrwania.Size = new System.Drawing.Size(148, 20);
            this.textBoxCzasTrwania.TabIndex = 8;
            // 
            // comboBoxOcena
            // 
            this.comboBoxOcena.FormattingEnabled = true;
            this.comboBoxOcena.Items.AddRange(new object[] {
            "brak",
            "*",
            "**",
            "***",
            "****",
            "*****"});
            this.comboBoxOcena.Location = new System.Drawing.Point(92, 132);
            this.comboBoxOcena.Name = "comboBoxOcena";
            this.comboBoxOcena.Size = new System.Drawing.Size(148, 21);
            this.comboBoxOcena.TabIndex = 9;
            this.comboBoxOcena.Text = "brak";
            this.comboBoxOcena.SelectedIndexChanged += new System.EventHandler(this.comboBoxOcena_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "dodaj plakat";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 325);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(228, 28);
            this.button2.TabIndex = 13;
            this.button2.Text = "Dodaj Film :v";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBoxPlakat);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(249, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 297);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Plakat:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Opis";
            // 
            // textBoxOpis
            // 
            this.textBoxOpis.Location = new System.Drawing.Point(58, 159);
            this.textBoxOpis.Multiline = true;
            this.textBoxOpis.Name = "textBoxOpis";
            this.textBoxOpis.Size = new System.Drawing.Size(182, 146);
            this.textBoxOpis.TabIndex = 16;
            // 
            // pictureBoxPlakat
            // 
            this.pictureBoxPlakat.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBoxPlakat.Image = global::BazaFilmow.Properties.Resources.brakzdjecia;
            this.pictureBoxPlakat.Location = new System.Drawing.Point(6, 60);
            this.pictureBoxPlakat.Name = "pictureBoxPlakat";
            this.pictureBoxPlakat.Size = new System.Drawing.Size(180, 229);
            this.pictureBoxPlakat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPlakat.TabIndex = 10;
            this.pictureBoxPlakat.TabStop = false;
            this.pictureBoxPlakat.WaitOnLoad = true;
            // 
            // DodajFilm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(451, 365);
            this.Controls.Add(this.textBoxOpis);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.comboBoxOcena);
            this.Controls.Add(this.textBoxCzasTrwania);
            this.Controls.Add(this.comboBoxGatunek);
            this.Controls.Add(this.textBoxRok);
            this.Controls.Add(this.textBoxTytul);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DodajFilm";
            this.Text = "DodajFilm!";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlakat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTytul;
        private System.Windows.Forms.TextBox textBoxRok;
        private System.Windows.Forms.ComboBox comboBoxGatunek;
        private System.Windows.Forms.TextBox textBoxCzasTrwania;
        private System.Windows.Forms.ComboBox comboBoxOcena;
        private System.Windows.Forms.PictureBox pictureBoxPlakat;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxOpis;
    }
}