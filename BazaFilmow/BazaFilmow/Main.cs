﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace BazaFilmow
{

    public partial class Main : Form
    {
        private List<Film> filmy = new List<Film>();

        public Main()
        {
            InitializeComponent();
        }

        public void DodaFilm(Film f)
        {
            filmy.Add(f);
        }

        public void DodajFilmDoDataGridView(Film f)
        {
            dataGridView1.Rows.Add(f.tytul, f.rok, f.gatunek, f.czastrwania, f.ocena);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            DodajFilm addFilmForm = new DodajFilm(this);
            DialogResult dr = addFilmForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                MessageBox.Show("Dodano film!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (filmy.Count == 0)
            {
                MessageBox.Show("Brak filmów na liście!");
            }
            else
            {
                int i = dataGridView1.CurrentCell.RowIndex;
                PokazInfo pokazInfoForm = new PokazInfo(filmy[i]);
                DialogResult dr = pokazInfoForm.ShowDialog();
            }
        }

        private void aboutBazaFilmówToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Baza filmów v.1 BETA\n\nAutorzy: \nŁukasz Fałda (falda.lukasz@gmail.com)\nBłażej Hap (blazejhap@gmail.com)", "O Bazie Filmów", MessageBoxButtons.OK);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chcesz zakończyć?", "Zamykanie programu", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*FileStream fs = new FileStream("./filmy.txt", FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                sw.WriteLine("[FILMY]");
                sw.WriteLine("[tytul]");
                sw.WriteLine(Convert.ToString(dataGridView1[0, i].Value));
                sw.WriteLine("[rok]");
                sw.WriteLine(Convert.ToString(dataGridView1[1, i].Value));
                sw.WriteLine("[gatunek]");
                sw.WriteLine(Convert.ToString(dataGridView1[2, i].Value));
                sw.WriteLine("[czas_trwania]");
                sw.WriteLine(Convert.ToString(dataGridView1[3, i].Value));
                sw.WriteLine("[ocena]");
                sw.WriteLine(Convert.ToString(dataGridView1[4, i].Value));
                sw.WriteLine("[FILMY_STOP]");
                sw.WriteLine();
            }
            sw.Close();*/
            FileStream fs = new FileStream("./filmy.txt", FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);

            for (int row = 0; row < filmy.Count; row++)
            {
                string lines = "";
                for (int col = 0; col < dataGridView1.ColumnCount; col++)
                {
                    lines += (string.IsNullOrEmpty(lines) ? "" : "") + dataGridView1.Rows[row].Cells[col].Value.ToString() + "##siema@@";
                }
                sw.Write(lines);
                sw.Write(filmy[row].plakat + "##siema@@");
                if (filmy[row].opis == "")
                {
                    sw.WriteLine("brak##siema@@");
                }
                else
                {
                    sw.WriteLine(filmy[row].opis + "##siema@@");
                }
            }
            sw.Close();
            MessageBox.Show("Zapis został wykonany pomyślnie.\nTwoja baza widnieje pod nazwą filmy.txt.", "zapis...");
        }

        private void nowaBazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < filmy.Count; ++i) { filmy.RemoveAt(i); }
            dataGridView1.Rows.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var lines = File.ReadAllLines("./filmy.txt");
            if (lines.Count() > 0)
            {
                foreach (var cellValues in lines.Skip(0))
                {
                   var cell = cellValues
                        .Split(new[] { "##siema@@" }, StringSplitOptions.RemoveEmptyEntries);
                    if (cell.Length-2 == dataGridView1.Columns.Count)
                        dataGridView1.Rows.Add(cell);


                }
                Film f = new Film(dataGridView1.Rows[0].Cells[0].Value.ToString(), Int32.Parse(dataGridView1.Rows[0].Cells[1].Value.ToString()), dataGridView1.Rows[0].Cells[2].Value.ToString(), dataGridView1.Rows[0].Cells[3].Value.ToString(), dataGridView1.Rows[0].Cells[4].Value.ToString(), ", ", "a tu opis"); // kurwa do poprawy, bo domyślne wchodzą w listę
                filmy.Add(f);
            }
            MessageBox.Show("Wczytano bazę z pliku filmy.txt.", "wczytywanie...");

            /*
             
             if (File.Exists("./filmy.txt"))
            {
                filmy.Clear();
                FileStream fs = new FileStream("./filmy.txt", FileMode.Open);
                StreamReader sr = new StreamReader(fs);
                string linia;

                string tyt = "";
                int ro = 0;
                string gat = "";
                string czatrw = "";
                string oce = "";
                string pla = "";
                string op = "";
                while ((linia = sr.ReadLine()) != null)
                {
                    if (linia == "[nr_rejestracyjny]")
                    {
                        tyt = sr.ReadLine();
                    }
                    else if (linia == "[marka]")
                    {
                        ro = Convert.ToInt32(sr.ReadLine());
                    }
                    else if (linia == "[rok_produkcji]")
                    {
                        gat = sr.ReadLine();
                    }
                    else if (linia == "[kolor]")
                    {
                        czatrw = sr.ReadLine();
                    }
                    else if (linia == "[ilosc_pasazerow]")
                    {
                        oce = sr.ReadLine();
                    }
                    else if (linia == "[END_SAMOCHOD]")
                    {
                        Film f = new Film(tyt, ro, gat, czatrw, oce, pla, op);
                        filmy.Add(f);
                        dataGridView1.Rows.Add(f.tytul, f.rok, f.gatunek, f.czastrwania, f.ocena);

                    }
                }
                sr.Close();
            }
            MessageBox.Show("Wczytano bazę z pliku filmy.txt.", "wczytywanie...");*/
        }
    }
}
